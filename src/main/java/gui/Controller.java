package gui;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.control.TextField;
import org.jnativehook.GlobalScreen;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;
import org.jnativehook.mouse.NativeMouseEvent;
import org.jnativehook.mouse.NativeMouseListener;

public class Controller{

    public TextField mouseClicksCountTextField;
    public TextField keyPressesCountTextField;

    private long mouseClicks;
    private long keyPresses;

    public Controller() {
        NativeKeyListener globalKeyListener = new NativeKeyListener() {
            public void nativeKeyPressed(NativeKeyEvent nativeKeyEvent) {
                keyPresses++;
                Platform.runLater(() -> updateView());
            }

            public void nativeKeyReleased(NativeKeyEvent nativeKeyEvent) { }

            public void nativeKeyTyped(NativeKeyEvent nativeKeyEvent) { }
        };

        NativeMouseListener globalMouseListener = new NativeMouseListener() {
            public void nativeMouseClicked(NativeMouseEvent nativeMouseEvent) {
                mouseClicks++;
                Platform.runLater(() -> updateView());
            }

            public void nativeMousePressed(NativeMouseEvent nativeMouseEvent) { }

            public void nativeMouseReleased(NativeMouseEvent nativeMouseEvent) { }
        };

        GlobalScreen.addNativeMouseListener(globalMouseListener);
        GlobalScreen.addNativeKeyListener(globalKeyListener);
    }

    private void updateView() {
        mouseClicksCountTextField.setText("" + mouseClicks);
        keyPressesCountTextField.setText("" + keyPresses);
    }

    public void resetButtonPressed(ActionEvent actionEvent) {
        mouseClicks = keyPresses = 0;
        updateView();
    }
}
